DIALOGFLOW COMPONENT EXPLANATION:

Intents:
--> This is where all the different training phrases for a given sentence is added.
--> An intent is the place where it holds contexts, training phrases, responses that can be used to develop a chatbot agent.


Follow Up Intents:
--> Follow up intents are added followed by the main intent. The condition is that follow up intents will be fired only when the main intent are asked.


Contexts:
--> Contexts come into play whenever the follow up intents are used.
--> The output context of one intent and its follow up are the same.
--> Whenever a particular output context is matched then all its follow up intents can be accessed.


Training phrases:
--> It will be inside the intents section.
--> All the different training sentences are added here


Responses:
--> Responses are the response to a particular intent whenever its matched.
--> There are two types
Types:
--> Text response: A simple text response will be given.
--> Custom payload: In this response structure will be json and it can be customised to our requirement. The documentation for custom payload is
given at custom_payload_documentation_reference.txt


Entities:
--> Entities are used to extract information from a sentence like name, date, numbers, amount, location, zipcode and much more.
--> Custom entities can also be added for our requirement
--> These entities are added in the training phase of an intent.


Integrations:
--> Integrations are used to integrate our agent with some other third party applications like Messenger, Telegram etc
--> We can also have a web demo by getting the link from web demo tab 


History:
--> History contains all the logs of the testing done in both the console and in the server.
--> We can get the reference of how our agent performs in real time from this tab



Small Talk:
--> Small talks are pre defined set of trained intents that can be added to our trained agent with the click of a button.
--> The agent will give preference to the small talk intents than our trained intents.
--> So it is recommended to not use small talk intents when our agent is very big or having many more intents.